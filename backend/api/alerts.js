const express = require("express");
const router = express.Router();
const mongo = require("mongodb");
const MongoClient = mongo.MongoClient;
const connectionUrl = process.env.MONGO_CONNECTION_STRING;

router.delete("/:id", (req, res) => {
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection alerts
      const db = client.db("gameWare");
      const collection = "alerts";

      let id;

      try {
        id = mongo.ObjectID(req.params.id);
      } catch {
        res.status(400).send("Invalid ID");
      }

      //Deletes all of the users alerts
      db.collection(collection).deleteMany(
        {
          playerId: id,
        },
        (err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          res.json({
            deletedCount: result.deletedCount,
          });
          client.close();
        }
      );
    }
  );
});

router.get("/:id", (req, res) => {
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection alerts
      const db = client.db("gameWare");
      const collection = "alerts";

      let id;

      try {
        id = mongo.ObjectID(req.params.id);
      } catch {
        res.status(400).send("Invalid ID");
      }

      // Simply fetches all of a users alerts
      db.collection(collection)
        .find({
          playerId: id,
        })
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          res.json(result);
          client.close();
        });
    }
  );
});

// Export API routes
module.exports = router;
