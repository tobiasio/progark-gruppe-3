const mongo = require("mongodb");

module.exports = {
  notifyPlayers: function (client, players, timeout, tournament, name) {
    // Goes through a list of players, and notifies them that the tournament is over. timeout boolean
    // is toogled if it is a timeout alert
    const objects = [];
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      if (players.length == 0) {
        resolve();
      }
      for (let i = 0; i < players.length; i++) {
        objects.push({
          playerId: mongo.ObjectID(players[i]),
          tournamentId: tournament,
          timeout: timeout,
          tournamentName: name,
        });
      }
      // Get the collection and bulk api artefacts
      var collection = db.collection("alerts"),
        bulkUpdateOps = [];

      objects.forEach(function (doc) {
        bulkUpdateOps.push({ insertOne: { document: doc } });
      });

      if (bulkUpdateOps.length > 0) {
        collection.bulkWrite(bulkUpdateOps).then(function (r) {
          resolve();
        });
      }
    });
  },

  endTournament: function (client, tournament) {
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      const collection = "tournaments";
      db.collection(collection).updateOne(
        { _id: tournament },
        { $set: { active: false } },
        (err, result) => {
          if (err) {
            resolve(500); // Internal server error
            return;
          }
          resolve(result);
          return;
        }
      );
    });
  },

  timeOut: function (client, tournament, players, rounds) {
    if (!players) {
      players = [];
    }
    // Times out all players in players by pulling them from the players list
    // and deleting their most recent round
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      db.collection("tournaments").updateOne(
        {
          _id: tournament,
        },
        {
          $pull: { players: { $in: players } },
          $inc: { currentPlayers: -players.length },
        },
        (err, result) => {
          if (err) {
            resolve(500); // Internal server error
            return;
          }
          db.collection("rounds").deleteMany(
            {
              _id: { $in: rounds },
            },
            (err, result) => {
              if (err) {
                resolve(500); // Internal server error
                return;
              }
              resolve(result);
              return;
            }
          );
        }
      );
    });
  },

  getCurrentRound: function (client, tournamentId) {
    // returns relevant info about the tournament, most importantly the round
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      const collection = "tournaments";

      db.collection(collection).findOne(
        {
          _id: tournamentId,
        },
        {
          currentRound: 1,
          name: 1,
          startTime: 1,
          maxPlayers: 1,
          currentPlayers: 1,
        },
        (err, result) => {
          if (err) {
            resolve(500); // Internal server error
            return;
          }
          resolve(result);
          return;
        }
      );
    });
  },

  newRound: function (
    client,
    tournamentId,
    players,
    gameId,
    roundNr,
    deadlineDate
  ) {
    //Creates a new round for all players in players array
    return new Promise((resolve, reject) => {
      // Using the database gameWare and collection rounds
      const db = client.db("gameWare");
      const collection = db.collection("rounds");
      let objects = [];

      for (let i = 0; i < players.length; i++) {
        objects.push({
          tournamentId: mongo.ObjectID(tournamentId),
          playerId: mongo.ObjectID(players[i]),
          gameId: mongo.ObjectID(gameId),
          roundNr: roundNr,
          deadlineDate: deadlineDate,
          scoreValue: 0,
          hasPlayed: false,
          tournamentPoints: 0,
        });
      }

      let bulkUpdateOps = [];

      objects.forEach(function (doc) {
        bulkUpdateOps.push({ insertOne: { document: doc } });
      });

      if (bulkUpdateOps.length > 0) {
        collection.bulkWrite(bulkUpdateOps).then(function (r) {
          resolve();
        });
      }
    });
  },

  updateTournamentRound: function (client, tournament) {
    // Checks if the currentRound is less than totalGames and if so increments it,
    // taking the tournament to next round
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      const collection = "tournaments";

      db.collection(collection)
        .findOneAndUpdate(
          {
            _id: tournament,
            $expr: {
              $lt: ["$currentRound", "$totalGames"],
            },
          },
          {
            $inc: { currentRound: 1 },
          },
          {
            returnOriginal: false,
          }
        )
        .then((updatedDocument) => {
          resolve(updatedDocument);
        });
    });
  },

  checkRounds: function (result, start, max, roundNr, current) {
    //Checks how many players have completed their rounds, timed out or still got time.
    return new Promise((resolve, reject) => {
      let timedOut = [];
      let timedOutRounds = [];
      let left = [];
      let amountComplete = 0;
      for (let i = 0; i < result.length; i++) {
        if (result[i].hasPlayed === true) {
          amountComplete++;
          left.push(result[i].playerId);
        } else if (new Date(result[i].deadlineDate) <= new Date()) {
          timedOut.push(result[i].playerId);
          timedOutRounds.push(result[i]._id);
        } else {
          left.push(result[i].playerId);
        }
      }
      // returns array[tournament can move on?, timed out players, players not timed out, rounds of timed out players]
      if (amountComplete == max) {
        // All players have completed their round
        resolve([true, [], left, []]);
      } else if (amountComplete == current && roundNr > 1) {
        // All players have completed their round
        resolve([true, [], left, []]);
      } else if (amountComplete > 1 && new Date() >= start && roundNr == 1) {
        // more than two have completed first round
        // startDelay passed.
        resolve([true, timedOut, left, timedOutRounds]);
      } else if (
        amountComplete + timedOut.length == current &&
        amountComplete >= 2 &&
        roundNr > 1
      ) {
        // More than two players have completed round and all players have either completed their round or timed out.
        resolve([true, timedOut, left, timedOutRounds]);
      } else {
        // Not enough people have completed their round
        resolve([false, timedOut, left, timedOutRounds]);
      }
    });
  },

  fetchRounds: function (client, tournamentId, activeRound) {
    // Fetches the rounds of a tournaments specific round number
    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      const collection = "rounds";

      db.collection("rounds")
        .find({
          tournamentId: tournamentId,
          roundNr: activeRound,
        })
        .sort({
          scoreValue: -1,
        })
        .toArray((err, result) => {
          if (err) {
            return;
          }
          resolve(result);
        });
    });
  },

  updateScore: function (client, id, placement, playerAmount) {
    // Updates the tournamentpoints for a specific round
    let tournamentPoints = (playerAmount - placement + 1) * 10;

    return new Promise((resolve, reject) => {
      const db = client.db("gameWare");
      const collection = "rounds";

      db.collection(collection)
        .findOneAndUpdate(
          {
            _id: mongo.ObjectID(id),
          },
          { $set: { tournamentPoints: tournamentPoints } },
          { returnNewDocument: true }
        )
        .then((updatedDocument) => {
          resolve(updatedDocument);
        });
    });
  },
};
