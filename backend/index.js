const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();

const port = process.env.PORT || 3002; // The port for the server

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((err, req, res, next) => {
  // This check makes sure this is a JSON parsing issue, but it might be
  // coming from any middleware, not just body-parser:

  if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
    return res.status(400).send("Invalid JSON"); // Bad request
  }

  next();
});

app.disable("x-powered-by"); // Minium security

app.use("/api/games", require("./api/games")); // Use games.js for route /api/games
app.use("/api/highscores", require("./api/highscores"));
app.use("/api/players", require("./api/players"));
app.use("/api/rounds", require("./api/rounds"));
app.use("/api/tournament", require("./api/tournament"));
app.use("/api/alerts", require("./api/alerts"));

// Default route
app.get("/api", (req, res) => {
  res.json({
    status: "API is working",
    message: "Welcome to the API for GameWare"
  });
});

app.listen(port, () => {
  // Start server on port
  console.log("Running express on port " + port);
  //console.log("Mongo Connection: " + process.env.MONGO_CONNECTION_STRING);
});
