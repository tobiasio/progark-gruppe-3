package com.gameware.game.sprites.pauseStateSprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// This is the button used for "Resume" and "Exit" in the pause state

public class PauseMenuButton extends Sprite {
    private Texture buttonTexture;

    public PauseMenuButton(Texture buttonTex, int x, int y, int width, int height){
        this.buttonTexture = buttonTex;
        this.width = width;
        this.height = height;

        this.position = new Vector3(x, y,0);
    }

    @Override
    public void reset() {
    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();
        sb.draw(this.buttonTexture, this.position.x, this.position.y, this.width, this.height);
        sb.end();
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void dispose() {
        this.buttonTexture.dispose();
    }

    public boolean isPressed(int xTouch, int yTouch){
        return xTouch > this.position.x && xTouch < (this.position.x + this.width) && yTouch > this.position.y && yTouch < (this.position.y + this.height);
    }
}
