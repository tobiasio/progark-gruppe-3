package com.gameware.game.sprites.pauseStateSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// These are the animated circles that can be seen in the background of the pause state

public class PauseCircle extends Sprite {
    private int radius;
    private Vector3 velocity;
    private ShapeRenderer sr;

    public PauseCircle(){
        this.position = new Vector3();
        this.velocity = new Vector3();

        this.sr = new ShapeRenderer();
        this.sr.setColor(new Color().set((float) (Math.random()), (float)(Math.random()), (float) (Math.random()),1f));

        this.radius = (int) (Math.random() * Gdx.graphics.getWidth() * 0.1 + Gdx.graphics.getWidth() * 0.01);

        this.velocity.x = (int) ((0.5 - Math.random()) * Gdx.graphics.getWidth() * 0.3);
        this.velocity.y = (int) ((0.5 - Math.random()) * Gdx.graphics.getHeight() * 0.3);
        this.position.x = (int) (Math.random() * (Gdx.graphics.getWidth() - this.radius*2) + this.radius);
        this.position.y = (int) (Math.random() * (Gdx.graphics.getHeight() - this.radius*2) + this.radius);
    }

    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        this.sr.begin(ShapeRenderer.ShapeType.Filled);

        this.sr.circle(this.position.x, this.position.y, this.radius);

        this.sr.end();
    }

    @Override
    public void update(float dt) {
        this.velocity.scl(dt);
        this.position.add(velocity.x, velocity.y, 0);
        this.velocity.scl(1 / dt);


        // Bounces off the edges
        if((this.position.x + this.radius) > Gdx.graphics.getWidth()){
            this.velocity.x = -Math.abs(this.velocity.x);
        }

        if((this.position.x - this.radius) < 0){
            this.velocity.x = Math.abs(this.velocity.x);
        }

        if((this.position.y + this.radius) > Gdx.graphics.getHeight()){
            this.velocity.y = -Math.abs(this.velocity.y);
        }

        if((this.position.y - this.radius) < 0){
            this.velocity.y = Math.abs(this.velocity.y);
        }
    }

    @Override
    public void dispose() {
        this.sr.dispose();
    }
}
