package com.gameware.game.sprites.fruitSlicerSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// This is the fruit that can be chopped in the Fruit Slicer minigame

public class Fruit extends Sprite {
    private Vector3 cutPosition1;
    private Vector3 cutPosition2;

    private Vector3 uncutVelocity;
    private Vector3 cutVelocity1;
    private Vector3 cutVelocity2;

    private int rotationSpeed;
    private int cutWidth;
    private int cutHeight;

    private float cuttingVelocity;
    private float gravity;

    private boolean isCut;

    private com.badlogic.gdx.graphics.g2d.Sprite uncutFruit;
    private com.badlogic.gdx.graphics.g2d.Sprite cutFruit1;
    private com.badlogic.gdx.graphics.g2d.Sprite cutFruit2;

    public Fruit(int x, int y, int width, int height, Vector3 velocity, Texture uncutFruitTexture){
        this.position = new Vector3(x, y, 0);
        this.cutPosition1 = new Vector3(0,0,0);
        this.cutPosition2 = new Vector3(0,0,0);

        this.cuttingVelocity = Gdx.graphics.getWidth()/2;
        this.gravity = Gdx.graphics.getWidth()/3;

        this.uncutVelocity = velocity;
        this.cutVelocity1 = new Vector3(-this.cuttingVelocity,0,0);
        this.cutVelocity2 = new Vector3(this.cuttingVelocity,0,0);

        this.rotationSpeed = (int) ((Math.random() - 0.5) * 1000);
        this.width = width;
        this.height = height;
        this.cutWidth = width/2;
        this.cutHeight = height;

        this.isCut = false;

        TextureRegion fullTexRegion = new TextureRegion(uncutFruitTexture);

        int regionCutWidth = fullTexRegion.getRegionWidth()/2;
        int regionCutHeight = fullTexRegion.getRegionHeight();

        TextureRegion leftTexRegion = new TextureRegion(fullTexRegion, 0,0, regionCutWidth, regionCutHeight);
        TextureRegion rightTexRegion = new TextureRegion(fullTexRegion, regionCutWidth,0, regionCutWidth, regionCutHeight);

        this.uncutFruit = new com.badlogic.gdx.graphics.g2d.Sprite(fullTexRegion);
        this.cutFruit1 = new com.badlogic.gdx.graphics.g2d.Sprite(leftTexRegion);
        this.cutFruit2 = new com.badlogic.gdx.graphics.g2d.Sprite(rightTexRegion);
    }




    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();

        if(this.isCut){
            this.cutFruit1.draw(sb);
            this.cutFruit2.draw(sb);
        }

        else{
            this.uncutFruit.draw(sb);
        }

        sb.end();
    }

    @Override
    public void update(float dt) {
        if(this.isCut){
            // Updates the position of the two fruit pieces based on its velocity

            this.cutVelocity1.scl(dt);
            this.cutVelocity2.scl(dt);

            this.cutPosition1.add(this.cutVelocity1.x, this.cutVelocity1.y, 0);
            this.cutPosition2.add(this.cutVelocity2.x, this.cutVelocity2.y, 0);

            this.cutVelocity1.scl(1/dt);
            this.cutVelocity2.scl(1/dt);

            // Sets the position of the two fruit pieces
            this.cutFruit1.setPosition(this.cutPosition1.x, this.cutPosition1.y);
            this.cutFruit2.setPosition(this.cutPosition2.x, this.cutPosition2.y);

            // Gravity affects the fruit pieces' velocity
            this.cutVelocity1.y -= this.gravity*2*dt;
            this.cutVelocity2.y -= this.gravity*2*dt;

            // Oppositely rotates the two pieces by a third of the original rotation speed
            this.cutFruit1.rotate(this.rotationSpeed / 3*dt);
            this.cutFruit2.rotate(-this.rotationSpeed / 3*dt);
        }

        else{
            // Updates the position of the full fruit based on its velocity
            this.uncutVelocity.scl(dt);
            this.position.add(this.uncutVelocity.x, this.uncutVelocity.y, 0);
            this.uncutVelocity.scl(1/dt);

            // Gravity affects the fruit's velocity
            this.uncutVelocity.y -= this.gravity*dt;

            // Sets the full fruit's size, position and rotation
            this.uncutFruit.setOriginCenter();
            this.uncutFruit.setSize(this.width, this.height);
            this.uncutFruit.setPosition(this.position.x, this.position.y);
            this.uncutFruit.rotate(this.rotationSpeed * dt);
        }
    }

    @Override
    public void dispose() {
        // Nothing to dispose
    }

    public boolean isDisposable() {
        // If the uncut fruit or the two cut fruit pieces have fallen this far in the y-direction, we know we can dispose it
        return this.position.y < -this.height*3 || (this.cutPosition1.y < -this.height*3 && this.cutPosition2.y < -this.height*3);
    }

    public boolean isPressed(int x, int y) {
        // If the user touched the fruit
        if(!this.isCut && x > this.position.x && x < (this.position.x + this.width) && (Gdx.graphics.getHeight() - y) > this.position.y && (Gdx.graphics.getHeight() - y) < (this.position.y + this.height)) {
            this.isCut = true;

            // Updates the sprite positions
            this.cutPosition1.x = this.position.x;
            this.cutPosition1.y = this.position.y;
            this.cutPosition2.x = this.position.x + this.cutWidth;
            this.cutPosition2.y = this.position.y;

            // Rotates the two velocities so the two parts of the fruit are moving away from each other
            this.cutVelocity1.rotate(this.uncutFruit.getRotation(), 0, 0, 1);
            this.cutVelocity2.rotate(this.uncutFruit.getRotation(), 0, 0, 1);

            // The two pieces keeps 25% of the original full fruits' velocity
            this.cutVelocity1.add((float)(this.uncutVelocity.x*0.25), (float)(this.uncutVelocity.y*0.25), 0);
            this.cutVelocity2.add((float)(this.uncutVelocity.x*0.25), (float)(this.uncutVelocity.y*0.25), 0);

            // Sets the size
            this.cutFruit1.setSize(this.cutWidth, this.cutHeight);
            this.cutFruit2.setSize(this.cutWidth, this.cutHeight);

            // Sets the origin of rotation the same as the original full fruit
            this.cutFruit1.setOrigin(cutWidth, cutHeight/2);
            this.cutFruit2.setOrigin(0, cutHeight/2);

            // Rotates the fruit similar to the full fruit
            this.cutFruit1.setRotation(this.uncutFruit.getRotation());
            this.cutFruit2.setRotation(this.uncutFruit.getRotation());

            return true;
        }
        return false;
    }
}
