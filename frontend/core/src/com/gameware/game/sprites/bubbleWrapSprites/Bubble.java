package com.gameware.game.sprites.bubbleWrapSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.gameware.game.GameWare;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

import java.util.ArrayList;
import java.util.Random;

// This is the bubble seen in the Bubble Wrap minigame

public class Bubble extends Sprite {
    private boolean textureChanged = false;
    private ArrayList<Texture> poppedTextures;
    private Texture unpoppedTex;
    private Sound popSound;

    private Texture currentTexture;

    public Bubble(int x, int y, int width, int height, Texture unpoppedTex, Texture poppedTex1, Texture poppedTex2, Texture poppedTex3) {
        this.position = new Vector3(x, y,0);
        this.width = width;
        this.height = height;
        this.popSound = Gdx.audio.newSound(Gdx.files.internal("sfx/pop.ogg"));

        // Add different textures for popped bubbles
        this.poppedTextures = new ArrayList<>();
        poppedTextures.add(poppedTex1);
        poppedTextures.add(poppedTex2);
        poppedTextures.add(poppedTex3);

        this.currentTexture = unpoppedTex;

        //For the reset method.
        this.unpoppedTex = unpoppedTex;
    }

    public boolean isPressed(int x, int y){
        return x > this.position.x && x < (this.position.x + this.width) && (Gdx.graphics.getHeight() - y) > this.position.y && (Gdx.graphics.getHeight() - y) < (this.position.y + this.height);
    }

    public void changeToPoppedTexture() {
        if(GameWare.getInstance().isSoundEffectsOn()){
            popSound.setVolume(popSound.play(),0.5f);
        }

        Random r = new Random();
        this.currentTexture = poppedTextures.get(r.nextInt(poppedTextures.size()));
        textureChanged = true;
    }

    public boolean isTextureChanged() {
        return textureChanged;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void reset() {
        textureChanged = false;
        this.currentTexture = unpoppedTex;
    }

    @Override
    public void draw(SpriteBatch sb) {
        Texture drawnTexture = this.currentTexture;
        sb.begin();
        sb.draw(drawnTexture,this.position.x,this.position.y,this.width,this.height);
        sb.end();
    }

    @Override
    public void update(float dt) {
    }

    public void dispose() {
        popSound.dispose();
        // Textures are shared so they should not be disposed
    }

}
