package com.gameware.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameware.game.states.State;

import java.util.Stack;

/*
    GameStateManager keeps track of which state is the current one. Delegates to the correct state.

    Patterns: State, Delegation, Update method
 */

public class GameStateManager {
    private Stack<State> states;

    private static GameStateManager instance = null;

    //Singleton (lazy initialization)
    public static GameStateManager getInstance(){
        if( instance == null){
            instance = new GameStateManager();
        }
        return instance;
    }

    private GameStateManager(){
        states = new Stack<State>();
    }

    public void push(State state){
        states.push(state);
    }

    public void pop(){
        states.pop().dispose();
    }

    public void set(State state){
        states.pop().dispose();
        states.push(state);
    }

//    Update method (delegates to current state)
    public void update(float dt){
        states.peek().update(dt);
    }

    public void render(SpriteBatch sb){
        states.peek().render(sb);
    }

    public Object report(){
        return states;
    }

    public void reset(){
        for(int i = 0; i < states.size(); i++){
            states.pop();
        }
    }

}
