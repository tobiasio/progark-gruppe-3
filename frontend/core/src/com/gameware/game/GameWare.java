package com.gameware.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import com.gameware.game.models.Game;
import com.gameware.game.models.LocalStorage;
import com.gameware.game.models.Player;
import com.gameware.game.states.menus.LoginState;
import com.gameware.game.states.menus.MenuState;
import com.gameware.game.states.games.PlayStateUnion;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*

	GameWare is the main class, it is the first class to be created. It is called from the AndroidLauncher.
	It extends LibGDX's ApplicationAdapter, and the render method (regarded as the game loop)
	delegates to the GameStateManager.

	Patterns: Singleton, Delegation, Game Loop
*/

public class GameWare extends ApplicationAdapter {
	private SpriteBatch batch;
	private GameStateManager gsm;

	public static int WIDTH = 480;
	public static int HEIGHT = 800;
	public static String TITLE;
	public static String skinFilePath;
	public static String localStorageFilePath;
	public static String apiBaseUrl;

	private Music music;
	private Boolean musicOn = false;
	private Boolean soundEffectsOn = true;
	private Boolean includeFin = false;
	private Player player;
	private List<Game> games = new ArrayList<>();

	private Map<String, PlayStateUnion> gameIdToPlayState = new HashMap<>();

	//Singleton (lazy initialization)
	private GameWare(){ }

	private static GameWare instance = null;

	public static GameWare getInstance(){
		if( instance == null){
			instance = new GameWare();
		}
		return instance;
	}

	@Override
	public void create () {
		gsm = GameStateManager.getInstance();

		loadGameConfigs();

		batch = new SpriteBatch();

		music = Gdx.audio.newMusic(Gdx.files.internal(("music/bensound-goinghigher.mp3")));
		music.setLooping(true);
		music.setVolume(0.1f);
		musicOn = true;

		// Local Storage
		readFromLocalStorage();
		if(player == null) {
			gsm.push(new LoginState(gsm));
		} else {
			gsm.push(new MenuState(gsm));
		}
		if(musicOn){music.play();}
	}

	@Override
	public void render () {
//		Game loop

//      Clearing the screen
	    Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//		Delegates to GameStateManager
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(batch);
	}

	@Override
	public void dispose () {
		batch.dispose();
		music.dispose();
	}

//	Option toggles
	public void toggleMusic(){
		if(musicOn){music.pause();}
		else{music.play();}
		musicOn = !musicOn;
		writeToLocalStorage();
	}

	public void toggleSoundEffects(){
		soundEffectsOn = !soundEffectsOn;
		writeToLocalStorage();
	}

//	Getters and setters
	public void setPlayer(Player player){
		this.player = player;
		writeToLocalStorage();
	}

	public Player getPlayer(){
		return player;
	}

	public List<Game> getGames() throws IOException {
		if(games.isEmpty()){
			games = QueryIntermediate.getGames();
		}
		return games;
	}

	public Map<String, PlayStateUnion> getGameIdToPlayState(){
		return gameIdToPlayState;
	}

	public void updateGameMap(String id, PlayStateUnion state){
		gameIdToPlayState.put(id, state);
	}

	public Boolean isMusicOn(){
		return musicOn;
	}

	public Boolean isSoundEffectsOn() {
		return soundEffectsOn;
	}

	public Boolean isIncludeFin(){ return includeFin; }

	public void setIncludeFin(Boolean t){
		includeFin = t;
		writeToLocalStorage();
	}

	/*
		The app uses local storage on the device to store the player and player options.
		The app writes to local storage when one of the stored variables change and reads from
		local storage when the app starts.
	 */
	private void writeToLocalStorage() {
		FileHandle file = Gdx.files.local(localStorageFilePath);
		Json json = new Json();
		json.setUsePrototypes(false);
		LocalStorage ls = new LocalStorage(player, musicOn, soundEffectsOn, includeFin);
		String lsString = json.toJson(ls);
		file.writeString(lsString, false);
	}

	private void readFromLocalStorage() {
		FileHandle file = Gdx.files.local(localStorageFilePath);
		if(!file.exists()) { return; }
		String fileOutput = file.readString();
		LocalStorage ls = new Json().fromJson(LocalStorage.class, fileOutput);
		if(ls.getPlayer() != null) { player = ls.getPlayer(); }
		if(ls.getMusicOn() != null) { musicOn = ls.getMusicOn(); }
		if(ls.getSoundEffects()!=null){ soundEffectsOn = ls.getSoundEffects(); }
		if(ls.getIncludeFin() != null) { includeFin = ls.getIncludeFin(); }
	}

	/*
		Shared, fixed variables are stored in config.json in the android/assets folder.
		The app reads the file on startup. In addition, the file contains the mapping from game id
		(from database) to the corresponding class in frontend
	 */
	private void loadGameConfigs() {
		FileHandle file = Gdx.files.internal("config.json");
		JsonValue base = new JsonReader().parse(file);
		TITLE = base.getString("TITLE");
		WIDTH = base.getInt("WIDTH");
		HEIGHT = base.getInt("HEIGHT");
		skinFilePath = base.getString("skinFilePath");
		localStorageFilePath = base.getString("localStorageFilePath");
		apiBaseUrl = base.getString("apiBaseUrl");

		// Add the mapping from game id to game class
		JsonValue games = base.get("games");
		JsonValue.JsonIterator iterator = games.iterator();
		while (iterator.hasNext()) {
			JsonValue game = iterator.next();
			String id = game.getString("id");
			String className = game.getString("class");
			try {
				Class cl = Class.forName(className);
				Constructor con = cl.getConstructor(GameStateManager.class);
				PlayStateUnion state = (PlayStateUnion) con.newInstance(gsm);
				gameIdToPlayState.put(id, state);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
