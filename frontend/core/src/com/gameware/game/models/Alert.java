package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

/*
    Model for the alerts about tournaments that finish
*/

public class Alert implements ModelInterface {
    private String _id;
    private String playerId;
    private String tournamentId;
    private boolean timeout;
    private String tournamentName;

    public Alert() {
    }

    public String get_id() {
        return _id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getTournamentId() {
        return tournamentId;
    }

    public boolean isTimeout() {
        return timeout;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    @Override
    public void reset() {
        _id = null;
        playerId = null;
        tournamentId = null;
        timeout = false;
        tournamentName = null;
    }

    @Override
    public String report() { return new Json().toJson(this); }

    @Override
    public String toString() {
        return report();
    }
}
