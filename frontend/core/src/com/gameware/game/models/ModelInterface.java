package com.gameware.game.models;

/*
    Data model interface for converting json objects to Java objects

    Patterns: Pipe-and-filter

    When implemented the class must be a model, in other words a data oriented java object.
    Data models used for the filters input/output ports in the QueryIntermediate's pipe-and-filter implementation.

    ModelInterface is also implementation of the testability tactic "Specialized interfaces" with
    requiring models to have a reset and a report method.
*/

public interface ModelInterface {
    void reset();
    String report();
}
