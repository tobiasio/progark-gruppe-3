package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

/*
    Model for the tournament points (TP). TP is the score a player has in a tournament
*/

public class Point implements ModelInterface {
    private String id; // playerId
    private String name;
    private int totalPoints;
    private int latestPoints;

    public Point() {
    }

    public Point(String id, String name, int totalPoints, int latestPoints) {
        this.id = id;
        this.name = name;
        this.totalPoints = totalPoints;
        this.latestPoints = latestPoints;
    }

    public String getName() {
        return name;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public int getLatestPoints() {
        return latestPoints;
    }

    public void reset() {
        name = null;
        totalPoints = Integer.parseInt(null);
        latestPoints = Integer.parseInt(null);
    }

    public String report() {
        return new Json().toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }
}
