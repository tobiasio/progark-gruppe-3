package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

import java.util.Comparator;

/*
    Model for the a high score a player gets on a specific game
*/

public class Highscore implements ModelInterface {
    private String _id;
    private String gameId;
    private String playerId;
    private String name;
    private double value;

    public Highscore() {
    }

    public Highscore(String _id, String gameId, String userId, String name, double value) {
        this._id = _id;
        this.gameId = gameId;
        this.playerId = userId;
        this.name = name;
        this.value = value;
    }

    public String get_id() {
        return _id;
    }

    public String getGameId() {
        return gameId;
    }

    public String getUserId() {
        return playerId;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public void reset() {
        _id = null;
        gameId = null;
        playerId = null;
        name = null;
        value = Double.parseDouble(null);
    }

    public String report() {
        return new Json().toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }

    public static Comparator<Highscore> hsValueComparator = new Comparator<Highscore>() {
        public int compare(Highscore hs1, Highscore hs2) {
            return (int) (hs2.getValue() - hs1.getValue());
        }};

}
