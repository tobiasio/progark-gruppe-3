package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

/*
    Model for checking and updating tournaments.
    If active is true, that means the tournament is not finished,
    if nextRound is true, that means the next round of the tournament is ready to be fetched and played.
    This is used inside PlayStateUnion to know which menu state is the next one after a game is over.
*/

public class RoundCheck implements ModelInterface {
   private boolean active;
   private boolean nextRound;

    public RoundCheck() {
    }

    public boolean isActive() {
        return active;
    }

    public boolean isNextRound() {
        return nextRound;
    }

    @Override
    public void reset() {
        active = false;
        nextRound = false;
    }

    @Override
    public String report() {
            Json json = new Json();
            json.setUsePrototypes(false);
            return json.toJson(this);
    }

    @Override
    public String toString() {
        return  report();
    }
}
