package com.gameware.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.gameware.game.GameStateManager;
import com.gameware.game.GameWare;

/*
    State is the super class/union of all the states in the application.

    A state has their own logic and rendering. State contains common variables between
    MenuStateUnion and PlayStateUnion. All states must have their own update method.

    Patterns: State, Union Design, Update method
*/

public abstract class State {


//    Data
    protected GameStateManager gsm;
    protected Stage stage = new Stage();
    protected final Skin skin = new Skin(Gdx.files.internal(GameWare.skinFilePath));

//    Sound effects
    protected Sound buttonPressSound;
    protected Sound pauseResumeBtnSound;
    protected Sound pauseStateBtnSound;


    protected State(GameStateManager gsm){
        this.gsm = gsm;

//        Add sound effects
        this.buttonPressSound = Gdx.audio.newSound(Gdx.files.internal("sfx/button_press.ogg"));
        this.pauseResumeBtnSound = Gdx.audio.newSound(Gdx.files.internal("sfx/pause_button.ogg"));
        this.pauseStateBtnSound = Gdx.audio.newSound(Gdx.files.internal("sfx/small_click.ogg"));

        Gdx.input.setInputProcessor(stage);
    }


//    Abstract state methods
    protected abstract void handleInput();

//    Requires all states to have the update method
    public abstract void update(float dt);

    public abstract void render(SpriteBatch sb);

    public abstract void dispose();

    public abstract void reset();
}
