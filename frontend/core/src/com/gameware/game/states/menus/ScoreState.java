package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gameware.game.GameWare;
import com.gameware.game.GameStateManager;
import com.gameware.game.states.State;

/*
    State after a game is over, showing the player which score they got on the game.
*/

public class ScoreState extends MenuStateUnion {

//    Data
    private State nextState;

//    Labels
    private final Label titleLabel = new Label("Your score:", skin, "big");
    private final Label continueLabel = new Label("Touch to continue", skin);
    private Label scoreLabel = new Label("N/A", skin, "big");

//    Variables
    private float currentTime = 0f;
    private  float scoreLabelDifferance;


    public ScoreState(GameStateManager gsm, int score, State nextState){
        super(gsm);

        this.nextState = nextState;
        scoreLabel.setText(score+"");
        continueLabel.getColor().a = 0;

//        GlyphLayout used to calculate the size of the label to scale it properly
        GlyphLayout g = new GlyphLayout();
        g.setText(skin.getFont("font-big"), score+"");
        scoreLabelDifferance = (g.width - (g.width*titleFontBigScale*2))/2;

        makeStage();
    }


    protected void makeStage(){
        Table rootTable = makeRootTable(backgroundScore);

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).spaceBottom(spacingLarge);
        rootTable.row();
        Container scoreContainer = new Container<Label>(scoreLabel);
        scoreContainer.setTransform(true);
        scoreContainer.align(Align.center);
        rootTable.add(scoreContainer).center().spaceBottom(spacingLarge);
        rootTable.row();
        rootTable.add(continueLabel);

        stage.addActor(rootTable);

//        Adds animations
        scoreContainer.addAction(Actions.sequence(
                new DelayAction(1f),
                Actions.parallel(
                        Actions.moveBy(scoreLabelDifferance, -(titleFontBigScale*50),2),
                        Actions.scaleTo(titleFontBigScale*2, titleFontBigScale*2, 2))));

        continueLabel.addAction(Actions.sequence(
                new DelayAction(2),
                Actions.forever(
                        new SequenceAction(
                                Actions.fadeIn(2),
                                Actions.fadeOut(2)))));
    }


    private void handleTouch(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(nextState);
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched()) {
            handleTouch();
        }
    }

    @Override
    public void update(float dt) {
        this.currentTime += dt;
        if(currentTime>2f){
            this.handleInput();
        }
        stage.act(dt);
    }
}
