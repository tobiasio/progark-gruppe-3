package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gameware.game.GameWare;
import com.gameware.game.models.Game;
import com.gameware.game.GameStateManager;
import com.gameware.game.states.games.PlayStateUnion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
    State where players can choose which game they want to play.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class SinglePlayerSelectGameState extends MenuStateUnion {

//    Data
    private List<Game> games = new ArrayList<>();

//    Texts
    private final String backBtnText = "Back";
    private final String playBtnText = "Play!";

//    Labels
    private final Label titleLabel = new Label("Single-Player", skin, "big");
    private final Label subHeadLabel = new Label("Select game", skin, "big");
    private final Label noGamesLabel = new Label("Something went wrong with getting the games", skin, "error");

//    Variables
    private final float scrollPaneWidth = Gdx.graphics.getWidth()/1.15f;
    private final float scrollPaneHeight = Gdx.graphics.getHeight()/2.3f;


    public SinglePlayerSelectGameState(GameStateManager gsm) {
        super(gsm);
        try {
            games = GameWare.getInstance().getGames();
        }catch(IOException e){
            System.out.println(e);
        }

        makeStage();
    }

    protected void makeStage(){
        Table rootTable = makeRootTable();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top();
        rootTable.row();
        rootTable.add(subHeadLabel);
        rootTable.row();
        ScrollPane scrollPane = new ScrollPane(makeInnerTable(), skin);
        rootTable.add(scrollPane);
        rootTable.getCell(scrollPane).size(scrollPaneWidth, scrollPaneHeight);
        rootTable.row();
        rootTable.add(makeBackBtn()).expand().bottom().left();

        stage.addActor(rootTable);
    }


//    Make widgets methods
    private Table makeInnerTable(){
        Table innerTable = new Table();
        innerTable.setBackground(backgroundTableBlueRounded);
        innerTable.columnDefaults(0).spaceRight(spacingLittle);

        if(games.isEmpty()){
//            If the try failed, and no games found
            innerTable.add(noGamesLabel).pad(padding);
        }else {
            for (Game g : games) {
                if(GameWare.getInstance().getGameIdToPlayState().get(g.getId()) == null){
                    continue;
                }
                innerTable.add(new Image(GameWare.getInstance().getGameIdToPlayState().get(g.getId()).getThumbnail())).width(imageWidthAndHeigh).height(imageWidthAndHeigh).pad(spacingLittle);

                Table innerInnerTable = new Table();
                innerInnerTable.add(makeTableWithLabelAndQuestionIcon(new Label(g.getName(), skin), makeQuestionIconDialog(new Label(g.getExplanation().replaceAll("\\\\n", "\n"), skin)))).spaceBottom(spacingLittle);
                innerInnerTable.row();

                TextButton gameBtn = new TextButton(playBtnText, makeTextButtonStyle(greenColor));
                gameBtn.addListener(new SinglePlayerSelectGameState.MyClickListener(g));
                innerInnerTable.add(gameBtn);

                innerTable.add(innerInnerTable);
                innerTable.row();
            }
        }
        return innerTable;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){ handleBackBtnClick(); }
        });
        return backBtn;
    }

    private void handleGameBtnClick(PlayStateUnion state){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(state);
    }

    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new MenuState(gsm));
    }

    public class MyClickListener extends ClickListener{
        private Game game;

        public MyClickListener(Game game){
            this.game = game;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            Map<String, PlayStateUnion> map = GameWare.getInstance().getGameIdToPlayState();
            PlayStateUnion s = map.get(game.getId());
            handleGameBtnClick(s);
        };
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }
}
