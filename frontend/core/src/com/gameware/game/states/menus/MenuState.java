package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gameware.game.GameWare;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;

/*
    The main menu state, where players can either choose sinple-player, multiplayer, high scores,
    options or log out.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class MenuState extends MenuStateUnion {

//    Labels
    private final Label titleLabel = new Label("GameWare", skin, "big");
    private final Label subHeadLabel = new Label("Welcome "+ GameWare.getInstance().getPlayer().getName()+"!", skin, "big");

//    Button texts
    private final String singlePlayerBtnText = "Single-Player";
    private final String multiPlayerBtnText = "Multiplayer";
    private final String highScoreBtnText = "High Scores";
    private final String optionBtnText = "Options";
    private final String logOutBtnText = "Log out";

    //    Textures
    private TextureRegionDrawable person = new TextureRegionDrawable(new TextureRegion(new Texture("menu/person.png")));
    private TextureRegionDrawable trophy = new TextureRegionDrawable(new TextureRegion(new Texture("menu/trophy.png")));
    private TextureRegionDrawable cog = new TextureRegionDrawable(new TextureRegion(new Texture("menu/cog.png")));
    private TextureRegionDrawable logOut = new TextureRegionDrawable(new TextureRegion(new Texture("menu/logOut.png")));

//    Sizes
    private final float personWidth = Gdx.graphics.getWidth()/25;
    private final float multiPersonWidth = Gdx.graphics.getWidth()/30;
    private final float trophyWidth = Gdx.graphics.getWidth()/25;
    private final float cogWidth = Gdx.graphics.getWidth()/20;
    private final float logOutWidth = Gdx.graphics.getWidth()/25;

    private boolean multiBtnClicked = false;
    private LoadingText loadingText = new LoadingText();


    public MenuState(GameStateManager gsm) {
        super(gsm);
        makeStage();
    }

    protected void makeStage(){
        Table rootTable = super.makeRootTable();

//        Add widgets
        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top();
        rootTable.row();
        rootTable.add(subHeadLabel).expandY();
        rootTable.row();
        rootTable.add(makeSinglePlayerBtn()).size(buttonWidth*1.3f, buttonHeight);
        rootTable.row();
        rootTable.add(makeMultiPlayerBtn()).size(buttonWidth*1.3f, buttonHeight).spaceBottom(spacingMedium);
        rootTable.row();

        Table innerTable = new Table();
        innerTable.add(makeHighScoreBtn()).size(buttonWidth*1.2f, buttonHeight).spaceBottom(spacingMedium).spaceRight(spacingLittle);
        innerTable.add(makeOptionsBtn()).size(buttonWidth*1.2f, buttonHeight).spaceBottom(spacingMedium);
        rootTable.add(innerTable).padBottom(spacingLarge);

        rootTable.row();
        rootTable.add(makeLogOutBtn()).expandX().bottom().left();

        stage.addActor(rootTable);
    }

//    Make widgets methods


    private Button makeSinglePlayerBtn(){
        Table table = new Table();
        table.add(new Image(person)).width(personWidth).height(buttonHeight/2).spaceRight(spacingLittle/2);
        table.add(new Label(singlePlayerBtnText, skin, "big")).spaceRight(spacingLittle/2);
        Button btn = new Button(table, skin);
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleSingleBtnClick();
            }
        });
        return btn;
    }

    private Button makeMultiPlayerBtn(){
        Table table = new Table();
        table.add(new Image(person)).width(multiPersonWidth).height(buttonHeight/2).spaceRight(spacingLittle/2);
        table.add(new Image(person)).width(multiPersonWidth).height(buttonHeight/2).spaceRight(spacingLittle/2);
        table.add(new Label(multiPlayerBtnText, skin, "big")).spaceRight(spacingLittle/2);
        Button btn = new Button(table, skin);
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setMultiBtnClicked();
            }
        });
        return btn;
    }

    private Button makeHighScoreBtn(){
        Table table = new Table();
        table.add(new Image(trophy)).width(trophyWidth).height(buttonHeight/2.5f).spaceRight(spacingLittle/2);
        table.add(new Label(highScoreBtnText, skin, "big")).spaceRight(spacingLittle/2);
        Button btn = new Button(table, skin);
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleHighscoreBtnClick();
            }
        });
        return btn;
    }

    private Button makeOptionsBtn(){
        Table table = new Table();
        table.add(new Image(cog)).width(cogWidth).height(buttonHeight/2.5f).spaceRight(spacingLittle/2);
        table.add(new Label(optionBtnText, skin, "big")).spaceRight(spacingLittle/2);
        Button btn = new Button(table, skin);
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleOptionsBtnClick();
            }
        });
        return btn;
    }

    private Button makeLogOutBtn(){
        Table table = new Table();
        table.add(new Image(logOut)).width(logOutWidth).height(buttonHeight/3f).spaceRight(spacingLittle/2);
        table.add(new Label(logOutBtnText, skin, "big")).spaceRight(spacingLittle/2);
        Button btn = new Button(table, makeTextButtonStyle(backColor));
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleLogOutBtnClick();
            }
        });
        return btn;
    }


    @Override
    public void update(float dt){
        super.update(dt);

        if(this.multiBtnClicked && this.loadingText.textIsRendering()){
            this.handleMultiBtnClick();
        }
        this.loadingText.update(dt);
    }

    @Override
    public void render(SpriteBatch sb){
        super.render(sb);
        this.loadingText.draw(sb);
    }

    private void setMultiBtnClicked(){
        this.multiBtnClicked = true;
        this.loadingText.setLoading();
    }


//    Handle click methods
    private void handleSingleBtnClick() {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new SinglePlayerSelectGameState(gsm));
    }

    private void handleMultiBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new CreateJoinTournamentState(gsm));
    }

    private void handleHighscoreBtnClick() {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new ViewHighScoreState(gsm));
    }

    private void handleOptionsBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new OptionsState(gsm));
    }

    private void handleLogOutBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new LoginState(gsm));
        GameWare.getInstance().setPlayer(null);
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleLogOutBtnClick();
        }
    }
}
