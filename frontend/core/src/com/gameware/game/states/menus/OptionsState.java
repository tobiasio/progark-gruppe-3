package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.gameware.game.GameWare;
import com.gameware.game.GameStateManager;

/*
    State where players can edit the options of the game

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons/checkboxes. Where P-S is used by utilizing
    click listeners and delegation is used by the button/checkbox to delegate what happens to this state.
*/

public class OptionsState extends MenuStateUnion {

//    Labels
    private final Label titleLabel = new Label("Options", skin, "big");
    private final Label musicToggleLabel = new Label("Music on/off", skin);
    private final Label soundEffectToggleLabel = new Label("Sound effects on/off", skin);

//    Texts
    private final String backBtnText = "Back";

//    Sizes
    private final float checkBoxSize = Gdx.graphics.getWidth()/14;


    public OptionsState(GameStateManager gsm) {
        super(gsm);

        makeStage();
    }

    protected void makeStage(){
//        Make root table
        Table rootTable = super.makeRootTable();
        rootTable.defaults().spaceBottom(spacingMedium).expandY();

//        Add title
        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).top();
        rootTable.row();

//        Make inner Table + adds labels and checkboxes
        Table innerTable = new Table();
        innerTable.pad(padding);
        innerTable.setBackground(backgroundTableBlueRounded);
        innerTable.defaults().spaceBottom(spacingMedium);
        innerTable.columnDefaults(0).spaceRight(spacingMedium);

        innerTable.add(musicToggleLabel);
        innerTable.add(makeMusicCheckBox());
        innerTable.row();
        innerTable.add(soundEffectToggleLabel);
        innerTable.add(makeSoundEffectCheckBox());
        rootTable.add(innerTable);
        rootTable.row();

        rootTable.add(makeBackBtn()).expandX().bottom().left();

        stage.addActor(rootTable);
    }

//    Make widgets methods
    private CheckBox makeMusicCheckBox(){
        CheckBox musicToggle = new CheckBox("",skin);
        musicToggle.getImage().setScaling(Scaling.fill);
        musicToggle.getImageCell().size(checkBoxSize);
        if((GameWare.getInstance().isMusicOn() && !musicToggle.isChecked()) || (!GameWare.getInstance().isMusicOn() && musicToggle.isChecked())){
            musicToggle.toggle();
        }
        musicToggle.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                if(GameWare.getInstance().isSoundEffectsOn()){ checkBoxSound.play(); }
                GameWare.getInstance().toggleMusic();
            }
        });
        return musicToggle;
    }

    private CheckBox makeSoundEffectCheckBox(){
        CheckBox soundEffectToggle = new CheckBox("",skin);
        soundEffectToggle.getImage().setScaling(Scaling.fill);
        soundEffectToggle.getImageCell().size(checkBoxSize);
        soundEffectToggle.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                GameWare.getInstance().toggleSoundEffects();
                if(GameWare.getInstance().isSoundEffectsOn()){ checkBoxSound.play(); }

            }
        });
        if((GameWare.getInstance().isSoundEffectsOn() && !soundEffectToggle.isChecked()) || (!GameWare.getInstance().isSoundEffectsOn() && soundEffectToggle.isChecked())){
            soundEffectToggle.toggle();
        }
        return soundEffectToggle;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClick();
            }
        });
        return backBtn;
    }


//    Handle click method
    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new MenuState(gsm));
    }


    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }

    @Override
    public void reset() {

        if(!GameWare.getInstance().isMusicOn()){
            GameWare.getInstance().toggleMusic();
        }
        if(!GameWare.getInstance().isSoundEffectsOn()){
            GameWare.getInstance().toggleSoundEffects();
        }
    }
}
