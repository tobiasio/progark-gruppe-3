package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Alert;
import com.gameware.game.models.Round;
import com.gameware.game.models.Tournament;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
    State where players can view, join and enter tournaments, or go to the create new state.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class CreateJoinTournamentState extends MenuStateUnion {

//    Data
    private List<Tournament> tournaments = new ArrayList<>();
    private List<Tournament> finTournaments = new ArrayList<>();
    private List<Alert> alerts = new ArrayList<>();

//    Labels
    private final Label titleLabel = new Label("Multiplayer", skin, "big");
    private final Label subHeadLabel = new Label("Currently playing", skin, "big");
    private final Label noTournamentsLabel = new Label("No tournaments", skin);
    private final Label nameColLabel = new Label("Name", skin, "big");
    private final Label statusColLabel = new Label("Status", skin, "big");
    private final Label includeFinTournamentsLabel = new Label("Include finished?", skin,"big");
    Label tournamentFeedbackLabel = new Label("", skin);

//    Texts
    private final String backBtnText = "Back";
    private final String createBtnText = "Create new";
    private final String joinBtnText = "Join new";
    private final String isPlayedTrueText = "Waiting";
    private final String isPlayedFalseText = "Your turn";
    private final String noAvailableTournamentsText = "No available tournaments";
    private final String leaveDialogText = "Are you sure want to\nleave ";
    private final String joinedText = "Joined ";
    private final String finishedText = "Finished";
    final String leftTournamentText = "Left ";


//    Variables
    private Tournament tournamentTryingToLeave = null;
    private boolean includeFinishedTournaments = GameWare.getInstance().isIncludeFin();
    private final float scrollPaneWidth = Gdx.graphics.getWidth()/1.15f;
    private final float scrollPaneHeight = Gdx.graphics.getHeight()/4f;

//    Dialogs
    private Dialog dialog;
    private CheckBox includeCheckBox;
    private Dialog alertDialog;

//      Loading text
    private boolean includeCheckboxClicked = false;
    private boolean leaveConfirmed = false;
    private boolean joinButtonClicked = false;
    private boolean enterButtonClicked = false;
    private Tournament enteredTournament;
    private Round enteredRound;

    private LoadingText loadingText = new LoadingText();

    public CreateJoinTournamentState(GameStateManager gsm) {
        super(gsm);

        try{
            tournaments = QueryIntermediate.getTournamentsForPlayer(GameWare.getInstance().getPlayer().getId(),true);
        }catch(Exception e){
            System.out.println(e);
        }
        try {
            finTournaments = QueryIntermediate.getTournamentsForPlayer(GameWare.getInstance().getPlayer().getId(),false);
        }catch(Exception e){
            System.out.println(e);
        }
        try{
            alerts = QueryIntermediate.getAlertsForPlayer(GameWare.getInstance().getPlayer().getId());
        } catch (IOException e){
            e.printStackTrace();
        }

        makeStage();

        if(!alerts.isEmpty()){
            try {
                QueryIntermediate.deleteAlertsForPlayer(GameWare.getInstance().getPlayer().getId());
            }catch (IOException e){
                System.out.println(e);
            }
            makeAlertDialog(alerts);
        }
    }

    protected void makeStage(){
        Table rootTable = makeRootTable();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top();
        rootTable.row();
        rootTable.add(subHeadLabel);
        rootTable.row();
        ScrollPane tournamentsPane = new ScrollPane(createTournamentList(), skin);
        rootTable.add(tournamentsPane);
        rootTable.getCell(tournamentsPane).size(scrollPaneWidth, scrollPaneHeight);
        rootTable.row();
        rootTable.add(tournamentFeedbackLabel);
        rootTable.row();
        Table btnTable = new Table();
        btnTable.add(includeFinTournamentsLabel).spaceBottom(spacingMedium);
        includeCheckBox = makeIncludeCheckbox();
        btnTable.add(includeCheckBox).spaceBottom(spacingMedium);
        btnTable.row();
        btnTable.add(makeCreateBtn()).size(buttonWidth, buttonHeight).spaceRight(spacingLittle);
        btnTable.add(makeJoinBtn()).size(buttonWidth, buttonHeight);
        rootTable.add(btnTable);
        rootTable.row();
        rootTable.add(makeBackBtn()).expand().bottom().left();

        stage.addActor(rootTable);

        makeLeaveDialog();
    }

//    Make widgets methods

    private void makeAlertDialog(List<Alert> alerts){
        alertDialog = new Dialog("", skin, "dialog") {
            public void result(Object obj) { }
        };
        String s = "Tournaments that finished:\n";
        for (Alert a : alerts){
            s += a.getTournamentName()+"\n";
        }
        Label l = new Label(s, skin);
        l.setAlignment(Align.center);
        alertDialog.text(l).pad(padding);
        alertDialog.button("Okay", true).pad(padding); //sends "true" as the result
        alertDialog.show(stage);
    }

    private void makeLeaveDialog(){
        dialog = new Dialog("", skin, "dialog") {
            public void result(Object obj) {
                if(obj.equals(true)){
                    setLeaveConfirmed();
                } else{
                    makeLeaveDialog();
                }

            }
        };
        dialog.text("");
        dialog.button("Yes", true); //sends "true" as the result
        dialog.button("No", false);  //sends "false" as the result
    }

    private Table createTournamentList(){
        Table innerTable = new Table();
        innerTable.setBackground(backgroundTableBlueRounded);
        innerTable.defaults().space(spacingLittle).expandY();
        innerTable.pad(spacingLittle).padRight(spacingLittle);

        if(!tournaments.isEmpty() || (!finTournaments.isEmpty() && includeFinishedTournaments) ) {
            innerTable.add(nameColLabel).expand().top();
            innerTable.add(statusColLabel).expand().top();
            innerTable.row();

            for (Tournament t : tournaments) {
                try{
                    Round r = QueryIntermediate.getRoundFromTournament(t.get_id(), GameWare.getInstance().getPlayer().getId(), t.getCurrentRound());

                    TextButton tournamentBtn = new TextButton(t.getName(), skin);
                    tournamentBtn.addListener(new EnterClickListener(t, r));
                    innerTable.add(tournamentBtn);

                    if(r.isPlayed()){
                        innerTable.add(new Label(isPlayedTrueText, skin));
                    }else{
                        innerTable.add(new Label(isPlayedFalseText, skin));
                    }
                }catch(Exception e){
                    System.out.println(e);
                    innerTable.add(new Label("N/A", skin));
                }
                Label leaveBtn = new Label("X", skin, "error");
                leaveBtn.setFontScale(4f);
                leaveBtn.addListener(new LeaveClickListener(t));
                innerTable.add(leaveBtn);
                innerTable.row();
            }

            //If the include checkbox is pressed, then the finished tournaments will render.
            if(includeFinishedTournaments){
                for (Tournament t : finTournaments) {
                    TextButton tournamentBtn = new TextButton(t.getName(), skin);
                    tournamentBtn.addListener(new EnterClickListener(t, null));
                    innerTable.add(tournamentBtn);

                    innerTable.add(new Label(finishedText, skin));

                    Label leaveBtn = new Label("X", skin, "error");
                    leaveBtn.setFontScale(4f);
                    leaveBtn.addListener(new LeaveClickListener(t));
                    innerTable.add(leaveBtn);
                    innerTable.row();
                }
            }
        }else{
            innerTable.add(noTournamentsLabel);
        }
        return innerTable;
    }

    private TextButton makeCreateBtn(){
        TextButton createBtn = new TextButton(createBtnText, skin);
        createBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleCreateBtnClick();
            }
        });
        return createBtn;
    }

    private TextButton makeJoinBtn(){
        TextButton joinBtn = new TextButton(joinBtnText, skin);
        joinBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setJoinButtonClicked();
            }
        });
        return joinBtn;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClick();
            }
        });
        return backBtn;
    }

    private CheckBox makeIncludeCheckbox(){
        final CheckBox includeCB = new CheckBox("",skin);
        includeCB.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setIncludeCheckboxClicked();
            }
        });
        includeCB.getImage().setScaling(Scaling.fill);
        includeCB.getImageCell().size(Gdx.graphics.getWidth()/16);
        includeCB.setChecked(includeFinishedTournaments);
        return includeCB;
    }


    @Override
    public void update(float dt){
        super.update(dt);

        // User pressed the checkbox
        if(this.includeCheckboxClicked && this.loadingText.textIsRendering()){
            this.handleIncludeCheckboxClick();
            this.includeCheckboxClicked = false;
        }

        // User left a tournament
        if(this.leaveConfirmed && this.loadingText.textIsRendering()){
            this.handleLeaveConfirmed();
            this.leaveConfirmed = false;
        }

        // User pressed the join button
        if(this.joinButtonClicked && this.loadingText.textIsRendering()){
            this.handleJoinBtnClick();
            this.joinButtonClicked = false;
        }

        // User pressed the enter tournament button
        if(this.enterButtonClicked && this.loadingText.textIsRendering()){
            this.handleEnterBtnClick(this.enteredTournament, this.enteredRound);
            this.enterButtonClicked = false;
            this.enteredTournament = null;
            this.enteredRound = null;
        }


        this.loadingText.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);

        this.loadingText.draw(sb);
    }

    private void setLeaveConfirmed(){
        this.leaveConfirmed = true;
        this.loadingText.setLoading();
    }

    private void setIncludeCheckboxClicked(){
        this.includeCheckboxClicked = true;
        this.loadingText.setLoading();
    }

    private void setJoinButtonClicked(){
        this.joinButtonClicked = true;
        this.loadingText.setLoading();
    }

    private void setEnterButtonClicked(Tournament clickedTournament, Round enteredRound){
        this.enterButtonClicked = true;
        this.enteredTournament = clickedTournament;
        this.enteredRound = enteredRound;
        this.loadingText.setLoading();
    }


//    Handle click methods

    private void handleLeaveConfirmed(){
        if (tournaments.contains(tournamentTryingToLeave)) {
            tournaments.remove(tournamentTryingToLeave);
        }else {
            finTournaments.remove(tournamentTryingToLeave);
        }
        try {
            QueryIntermediate.leaveTournament(tournamentTryingToLeave.get_id(), GameWare.getInstance().getPlayer().getId());

            stage.clear();
            setFeedbackLabelText(leftTournamentText+tournamentTryingToLeave.getName());
            makeStage();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void handleIncludeCheckboxClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ checkBoxSound.play(); }
        includeFinishedTournaments = !includeFinishedTournaments;
        GameWare.getInstance().setIncludeFin(includeFinishedTournaments);
        stage.clear();
        makeStage();
    }

    private void handleEnterBtnClick(Tournament t, Round r){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }

        if(!t.isActive()){
            gsm.set(new FinishedTournamentState(gsm,t));
        }
        else{
            gsm.set(new ViewTournamentState(gsm, t, r));
        }
    }

    private void handleLeaveBtnClick(Tournament t){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        Label l = new Label(leaveDialogText+t.getName()+"?\n", skin);
        l.setAlignment(Align.center);
        dialog.text(l).pad(padding);
        dialog.show(stage);
        tournamentTryingToLeave = t;
    }

    private void handleCreateBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new CreateNewTournamentState(gsm));
    }

    private void handleJoinBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        try {
            Tournament newT = QueryIntermediate.joinATournament(GameWare.getInstance().getPlayer().getId());
            tournaments.add(newT);

            setFeedbackLabelText(joinedText+newT.getName());

            stage.clear();
            makeStage();
        }catch(Exception e){
            e.printStackTrace();
            setFeedbackLabelText(noAvailableTournamentsText);
        }
    }

    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new MenuState(gsm));
    }


//    Click listeners
    public class EnterClickListener extends ClickListener{
        private Tournament tournament;
        private Round round;

        public EnterClickListener(Tournament tournament, Round round){
            this.tournament = tournament;
            this.round = round;
        }

        public void clicked(InputEvent event, float x, float y) {
            setEnterButtonClicked(tournament, round);
        };
    }

    public class LeaveClickListener extends ClickListener{
        private Tournament tournament;

        public LeaveClickListener(Tournament tournament){
            this.tournament = tournament;
        }

        public void clicked(InputEvent event, float x, float y) {
            handleLeaveBtnClick(tournament);
        };
    }

    void setFeedbackLabelText(String text){
        tournamentFeedbackLabel.getColor().a = 1;
        tournamentFeedbackLabel.addAction(Actions.sequence(
                new DelayAction(8),
                Actions.fadeOut(2)));
        tournamentFeedbackLabel.setText(text);
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }

    @Override
    public void reset() {
        tournaments.clear();
        finTournaments.clear();
        includeCheckBox.setChecked(false);
        try {
            tournaments = QueryIntermediate.getTournamentsForPlayer(GameWare.getInstance().getPlayer().getId(),true);
            finTournaments = QueryIntermediate.getTournamentsForPlayer(GameWare.getInstance().getPlayer().getId(),false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        makeStage();
    }
}
