package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Game;
import com.gameware.game.models.Highscore;
import com.gameware.game.GameStateManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/*
    State where players can view high scores for a specified game

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class ViewHighScoreForGameState extends MenuStateUnion {

//    Data
    private String playerId = GameWare.getInstance().getPlayer().getId();
    private List<Highscore> highscores = new ArrayList<>();

//    Labels
    private final Label titleLabel = new Label("Global High Scores", skin, "big");
    private final Label noHSLabel = new Label("No high scores recorded.", skin);
    private final Label placeLabel = new Label("Place", skin, "big");
    private final Label nameLabel = new Label("Name", skin, "big");
    private final Label scoreLabel = new Label("Score", skin, "big");
    private final Label playerLabel = new Label("Your score: ", skin);

    private Label gameNameLabel = new Label("N/A", skin, "big");
    private Label playerScoreLabel = new Label("0",skin);

//    Texts
    private final String backBtnText = "Back";

//    Variables
    private Boolean isNoHS = true;
    private final int numerOfPlacesShown = 5;
    private final float scrollPaneWidth = Gdx.graphics.getWidth()/1.25f;
    private final float scrollPaneHeight = Gdx.graphics.getHeight()/3.5f;
    private final float playerScoreWidth = Gdx.graphics.getWidth()/2f;
    private final float playerScoreHeight = Gdx.graphics.getHeight()/10f;


    protected ViewHighScoreForGameState(GameStateManager gsm, Game game) {
        super(gsm);
        gameNameLabel.setText(game.getName());

        try{
            highscores = QueryIntermediate.getHighscoresForGame(game.getId());
            isNoHS = false;

            int playerHS = (int) QueryIntermediate.getHighscoreForPlayerAndGame(playerId,game.getId()).getValue();
            playerScoreLabel.setText(String.valueOf(playerHS));
        } catch(NoSuchElementException | IOException e){
            e.printStackTrace();
        }

        makeStage();
    }

    protected void makeStage() {
        Table rootTable = super.makeRootTable();
        rootTable.defaults().expandY();

        titleLabel.setFontScale(tinierTitleFontBigScale);
        rootTable.add(titleLabel);
        rootTable.row();
        gameNameLabel.setFontScale(tinierTitleFontBigScale);
        rootTable.add(gameNameLabel).top();
        rootTable.row();
        ScrollPane hsPane = new ScrollPane(createHighScoreList(), skin);
        rootTable.add(hsPane);
        rootTable.getCell(hsPane).size(scrollPaneWidth, scrollPaneHeight);
        rootTable.row();
        Table secondInnerTable = new Table();
        secondInnerTable.pad(padding);
        secondInnerTable.setBackground(backgroundTableBlueRounded);

        secondInnerTable.add(playerLabel).spaceRight(spacingLittle);
        secondInnerTable.add(playerScoreLabel);
        rootTable.add(secondInnerTable);
        rootTable.getCell(secondInnerTable).size(playerScoreWidth, playerScoreHeight);
        rootTable.row();

        rootTable.add(makeBackBtn()).expandX().bottom().left();

        stage.addActor(rootTable);
    }


//    Make widgets methods
    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClick();
            }
        });
        return backBtn;
    }

    private Table createHighScoreList() {
        Table innerTable = new Table();
        innerTable.setBackground(backgroundTableBlueRounded);
        innerTable.defaults().spaceRight(spacingLittle);

        if(isNoHS){
            innerTable.add(noHSLabel).pad(padding);
        } else {
            innerTable.add(placeLabel).top();
            innerTable.add(nameLabel).top();
            innerTable.add(scoreLabel).top();
            innerTable.row();

            int length = Math.min(highscores.size(), numerOfPlacesShown);

            for (int i = 0; i < length; i++){
                Highscore hs = highscores.get(i);

                Label placementLabel = new Label(String.valueOf(i+1)+". ",skin);
                Label nameLabel = new Label(hs.getName()+" ",skin);
                Label scoreLabel = new Label(String.valueOf((int) hs.getValue()),skin);

                innerTable.add(placementLabel);
                innerTable.add(nameLabel);
                innerTable.add(scoreLabel);
                innerTable.row();
            }
        }
        return innerTable;
    }


//    Handle click method
    private void handleBackBtnClick() {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new ViewHighScoreState(gsm));
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }
}