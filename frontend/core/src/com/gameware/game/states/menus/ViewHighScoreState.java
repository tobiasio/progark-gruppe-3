package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gameware.game.GameWare;
import com.gameware.game.models.Game;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;

import java.io.IOException;

/*
    State where players can choose which game they want to view high scores for

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class ViewHighScoreState extends MenuStateUnion {

//    Data
    private List<Game> games = new ArrayList<>();

//    Labels
    private final Label titleLabel = new Label("High Scores", skin, "big");
    private final Label subHeadLabel = new Label("Select game", skin, "big");
    private final Label noGamesLabel = new Label("Something went wrong with getting the games", skin, "error");

//    Texts
    private final String backBtnText = "Back";
    private final String BtnText = "View High Score";

//    Values
    private final float scrollPaneWidth = Gdx.graphics.getWidth()/1.15f;
    private final float scrollPaneHeight = Gdx.graphics.getHeight()/2.3f;
    private final float imageWidthAndHeigh = Gdx.graphics.getWidth()/4;

//    Loading text
    private Game chosenGame = null;
    private LoadingText loadingText = new LoadingText();


    public ViewHighScoreState(GameStateManager gsm) {
        super(gsm);

        try{
            games = GameWare.getInstance().getGames();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        makeStage();
    }

    protected void makeStage(){
        Table rootTable = super.makeRootTable();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top();
        rootTable.row();
        rootTable.add(subHeadLabel);
        rootTable.row();
        ScrollPane scrollPane = new ScrollPane(makeInnerTable(), skin);
        rootTable.add(scrollPane).spaceBottom(spacingLittle);
        rootTable.getCell(scrollPane).size(scrollPaneWidth, scrollPaneHeight);
        rootTable.row();
        rootTable.add(makeBackBtn()).expandY().bottom().left();

        stage.addActor(rootTable);
    }

//    Make widgets methods
    private Table makeInnerTable(){
        Table innerTable = new Table();
        innerTable.setBackground(backgroundTableBlueRounded);

        if(games.size()==0){
//            If the try failed, and no games found
            innerTable.add(noGamesLabel).pad(padding);
        }else {
            for (Game g : games) {
                if(GameWare.getInstance().getGameIdToPlayState().get(g.getId()) == null){
                    continue;
                }
                innerTable.add(new Image(GameWare.getInstance().getGameIdToPlayState().get(g.getId()).getThumbnail())).width(imageWidthAndHeigh).height(imageWidthAndHeigh).pad(spacingLittle);

                Table innerInnerTable = new Table();
                innerInnerTable.add(makeTableWithLabelAndQuestionIcon(new Label(g.getName(), skin), makeQuestionIconDialog(new Label(g.getExplanation().replaceAll("\\\\n", "\n"), skin)))).spaceBottom(spacingLittle);
                innerInnerTable.row();

                TextButton gameBtn = new TextButton(BtnText, skin);
                gameBtn.addListener(new ViewHighScoreState.MyClickListener(g));
                innerInnerTable.add(gameBtn);

                innerTable.add(innerInnerTable);
                innerTable.row();
            }
        }
        return innerTable;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){ handleBackBtnClick(); }
        });
        return backBtn;
    }


//    Handle click methods
    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new MenuState(gsm));
    }

    private void handleGameBtnClick(Game game) {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new ViewHighScoreForGameState(gsm, game));
    }


//    Listener
    public class MyClickListener extends ClickListener{
        private Game game;

        public MyClickListener(Game game){
            this.game = game;
        }

        public void clicked(InputEvent event, float x, float y) {
            setGameBtnClicked(game);
        }
    }

    private void setGameBtnClicked(Game game){
        this.chosenGame = game;
        this.loadingText.setLoading();
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);
        this.loadingText.draw(sb);
    }

    @Override
    public void update(float dt){
        super.update(dt);

        if(this.chosenGame != null && this.loadingText.textIsRendering()){
            this.handleGameBtnClick(this.chosenGame);
            this.chosenGame = null;
        }
        this.loadingText.update(dt);
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        this.loadingText.dispose();
    }
}