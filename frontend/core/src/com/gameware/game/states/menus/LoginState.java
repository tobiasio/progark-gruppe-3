package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Player;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;

import java.io.IOException;
import java.util.NoSuchElementException;

/*
    State where players can log in or create new user. This state contains two pages, to not
    many unnecessary many states.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class LoginState extends MenuStateUnion {

//    Labels
    private final Label titleLabel = new Label("GameWare", skin, "big");
    private final Label subHeadLabelLogIn = new Label("Log in", skin, "big");
    private final Label subHeadLabelCreateUser = new Label("Create User", skin, "big");
    private Label errorLabel = new Label("", skin, "error");

//    Input fields
    private String usernameInputText = "Username";
    private TextField usernameInputField;
    private String passwordInputText = "Password";
    private TextField passwordInputField;
    private String confirmPasswordInputText = "Confirm password";
    private TextField confirmPasswordInputField;
    private final char passwordCharacter = '*';
    private final int inputFieldWidth = Gdx.graphics.getWidth()/2;
    private final int inputFieldHeight = Gdx.graphics.getHeight()/15;

//    Button texts
    private final String loginBtnText = "Log in";
    private final String createUserText = "Create User";
    private final String signUpBtnText = "Sign Up";
    private final String backText = "Back";

//    Feedback texts
    private final String wrongLoginText = "User not found";
    private final String takenUsernameText = "Username already taken";
    private final String ioExceptionText = "Something went wrong with query";
    private final String passwordNotMatchingText = "Passwords do not match";

//    Loading text
    private LoadingText loadingText = new LoadingText();
    private boolean loginBtnClicked = false;
    private boolean signUpBtnClicked = false;


//    Variables
    private int page = 0;


    public LoginState(GameStateManager gsm) {
        super(gsm);
        makeStage();
    }

    protected void makeStage(){
        Table rootTable = super.makeRootTable();

//        Add widgets
        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top();
        rootTable.row();

        if (page == 0) {
            rootTable.add(subHeadLabelLogIn).expandY();
            rootTable.row();
            rootTable.add(makeUserInputField()).size(inputFieldWidth, inputFieldHeight);
            rootTable.row();
            rootTable.add(makePasswordInputField()).size(inputFieldWidth, inputFieldHeight);
            rootTable.row();
            rootTable.add(errorLabel);
            rootTable.row();
            rootTable.add(makeLoginBtn()).size(buttonWidth, buttonHeight);
            rootTable.row();
            rootTable.add(makeCreateUserBtn()).size(buttonWidth, buttonHeight).padBottom(spacingMedium).expandY().top();
        } else if (page == 1){
            rootTable.add(subHeadLabelCreateUser).expandY().spaceBottom(spacingMedium);
            rootTable.row();
            rootTable.add(makeUserInputField()).size(inputFieldWidth, inputFieldHeight);
            rootTable.row();
            rootTable.add(makePasswordInputField()).size(inputFieldWidth, inputFieldHeight);
            rootTable.row();
            rootTable.add(makeConfirmPasswordInputField()).size(inputFieldWidth, inputFieldHeight);
            rootTable.row();
            rootTable.add(errorLabel);
            rootTable.row();
            rootTable.add(makeSignUpBtn()).size(buttonWidth, buttonHeight);
            rootTable.row();
            rootTable.add(makeBackBtn()).expand().bottom().left();
        }

        removeKeyPadAtTouch();

        stage.addActor(rootTable);
    }

//    Make widgets methods
    private TextField makeUserInputField( ){
        usernameInputField = new TextField(usernameInputText, skin);
        usernameInputField.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(usernameInputField.getText().equals(usernameInputText)) {
                    usernameInputField.setText("");
                }
            }
        });

        usernameInputField.setFocusTraversal(false);
        return usernameInputField;
    }

    private TextField makePasswordInputField( ){
        passwordInputField = new TextField(passwordInputText, skin);
        passwordInputField.setPasswordCharacter(passwordCharacter);
        passwordInputField.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(passwordInputField.getText().equals(passwordInputText)){
                    passwordInputField.setText("");
                    passwordInputField.setPasswordMode(true);
                }
            }
        });
        return passwordInputField;
    }

    private TextField makeConfirmPasswordInputField( ){
        confirmPasswordInputField = new TextField(confirmPasswordInputText, skin);
        confirmPasswordInputField.setPasswordCharacter(passwordCharacter);
        confirmPasswordInputField.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(confirmPasswordInputField.getText().equals(confirmPasswordInputText)){
                    confirmPasswordInputField.setText("");
                    confirmPasswordInputField.setPasswordMode(true);
                }
            }
        });
        return confirmPasswordInputField;
    }

    private TextButton makeLoginBtn( ){
        TextButton loginBtn = new TextButton(loginBtnText, skin);
        loginBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setLoginBtnClicked();
            }
        });
        return loginBtn;
    }

    private TextButton makeCreateUserBtn( ){
        TextButton createUserBtn = new TextButton(createUserText, skin);
        createUserBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                page ++;
                errorLabel.setText("");
                stage.clear();
                makeStage();
            }
        });
        return createUserBtn;
    }

    private TextButton makeSignUpBtn( ){
        TextButton signUpBtn = new TextButton(signUpBtnText, skin);
        signUpBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                try{
                    isValidPassword();
                    setSignUpBtnClicked();
                }catch(IllegalArgumentException exception){
                    errorLabel.setText(passwordNotMatchingText);
                }

            }
        });
        return signUpBtn;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClicked();
            }
        });
        return backBtn;
    }


//    Handle click methods
    private void handleLoginBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }

        String username = usernameInputField.getText();
        String password = passwordInputField.getText();

        //Check if username = Username
        if(username.equals("Username")){
            errorLabel.setText(wrongLoginText);
            return;
        }

        try {
            Player player = QueryIntermediate.loginPlayer(username, password);

//            Removes keyboard
            Gdx.input.setOnscreenKeyboardVisible(false);
            stage.unfocusAll();

            GameWare.getInstance().setPlayer(player);
            gsm.set(new MenuState(gsm));
        }catch(Exception e) {
            e.printStackTrace();

//            Different feedback text depending on which exception
            if (e instanceof NoSuchElementException) {
                errorLabel.setText(wrongLoginText);
            } else if (e instanceof IOException) {
                errorLabel.setText(ioExceptionText);
            } else {
                errorLabel.setText(e.getMessage());
            }
        }
    }

    private void handleSignUpBtnClick() {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }

        String username = usernameInputField.getText();
        String password = passwordInputField.getText();
        try {
            Player player = QueryIntermediate.createNewPlayer(username, password);

//            Removes keyboard
            Gdx.input.setOnscreenKeyboardVisible(false);
            stage.unfocusAll();

            GameWare.getInstance().setPlayer(player);
            gsm.set(new MenuState(gsm));
        } catch (Exception e) {
            e.printStackTrace();

//            Different feedback text depending on which exception
            if (e instanceof NoSuchElementException) {
                errorLabel.setText(takenUsernameText);
            } else if (e instanceof IOException) {
                errorLabel.setText(takenUsernameText);
            } else {
                errorLabel.setText(e.getMessage());
            }
        }
    }

    private void handleBackBtnClicked(){
        page --;
        errorLabel.setText("");
        stage.clear();
        makeStage();
    }

//    Adds listener to stage that removes keyboard on touch
    private void removeKeyPadAtTouch(){
        stage.getRoot().addCaptureListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if (!(event.getTarget() instanceof TextField)){
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    stage.unfocusAll();
                }
                return false;
            }});
    }


    private void isValidPassword() throws IllegalArgumentException {
        if(!passwordInputField.getText().equals(confirmPasswordInputField.getText())){
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void update(float dt) {
        super.update(dt);

        if(this.loginBtnClicked && this.loadingText.textIsRendering()){
            this.handleLoginBtnClick();
            this.loginBtnClicked = false;
        }

        if(this.signUpBtnClicked && this.loadingText.textIsRendering()){
            this.handleSignUpBtnClick();
            this.signUpBtnClicked = false;
        }

        this.loadingText.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);

        this.loadingText.draw(sb);
    }

    private void setLoginBtnClicked(){
        this.loginBtnClicked = true;
        this.loadingText.setLoading();
    }

    private void setSignUpBtnClicked(){
        this.signUpBtnClicked = true;
        this.loadingText.setLoading();
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) && page == 1){
            handleBackBtnClicked();
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void reset() {
        usernameInputField.setText(usernameInputText);
        passwordInputField.setText(passwordInputText);
        confirmPasswordInputField.setText(confirmPasswordInputText);
        errorLabel.setText("");
    }
}
