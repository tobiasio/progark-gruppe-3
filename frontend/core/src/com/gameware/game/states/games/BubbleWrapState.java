package com.gameware.game.states.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameware.game.sprites.bubbleWrapSprites.Bubble;
import com.gameware.game.GameStateManager;

import java.util.ArrayList;

public class BubbleWrapState extends PlayStateUnion {
    private Texture background;
    private Texture unpopped;
    private Texture popped1;
    private Texture popped2;
    private Texture popped3;

    private ArrayList<Bubble> bubbles = new ArrayList<>();

    private int poppedBubbles;
    private int score;
    private final int bubbleColumns = 6;
    private final int bubbleRows = (int) (Math.floor(bubbleColumns * 1.5));
    private final float bubbleScale = 0.9f;
    private final int bubbleLength = Gdx.graphics.getWidth() / bubbleColumns;
    private final int bubbleXOffset = (int) ((1-this.bubbleScale) * this.bubbleLength / 2);

    public BubbleWrapState(GameStateManager gsm) {
        super(gsm);
        super.setPauseButtonWhite();
        super.setTotalTimeLimit(20f);
        super.setFontColorWhite();
        super.setThumbnail(new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubbleWrapPhotoEdit.png")));

        background = new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubblewrap_background.jpg"));
        unpopped = new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubble_unpopped_1.png"));
        popped1 = new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubble_popped_1.png"));
        popped2 = new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubble_popped_2.png"));
        popped3 = new Texture(Gdx.files.internal("gameTextures/BubbleWrap/bubble_popped_3.png"));

        this.poppedBubbles = 0;

        // Fill the table with bubbles
        for(int i = 0; i<this.bubbleRows; i++){
            for (int j = 0; j < this.bubbleColumns; j++) {
                bubbles.add(new Bubble(j*bubbleLength + this.bubbleXOffset,i*bubbleLength + 2*this.bubbleXOffset, (int) (bubbleLength * this.bubbleScale), (int) (bubbleLength * this.bubbleScale), unpopped, popped1, popped2, popped3));
            }
        }
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched()) {
            /* Checks if any bubble has been pressed and that it's not added to the counter more than once */
            for (Bubble bubble : bubbles) {
                if (bubble.isPressed(Gdx.input.getX(),Gdx.input.getY()) && !bubble.isTextureChanged()) {
                    bubble.changeToPoppedTexture();
                    poppedBubbles++;
                }
            }

            // Keeps score consistent
            // One point per bubble popped, plus three points for each second of remaining time. Time bonus is only given if the player pops all the bubbles.
            if(this.poppedBubbles == this.bubbles.size()){
                score = this.poppedBubbles + Math.max(Math.round((super.getTotalTimeLimit() - super.getCurrentDuration())*3), 0);
            }
            else{
                score = this.poppedBubbles;
            }
            this.setScore(score);
        }
    }

    @Override
    public void update(float dt) {
        // Effectively updates the pause button and the loading text
        super.update(dt);

        this.handleInput();

        // Game should finish if all the bubbles are popped.
        if(poppedBubbles == this.bubbles.size()) {
            super.setGameFinished();
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();
        sb.draw(this.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        sb.end();

        for (Bubble bubble : this.bubbles){
            bubble.draw(sb);
        }

        //Effectively renders the pause button, the loading text, the score text and the current duration text
        super.render(sb);
    }


    @Override
    public void dispose() {
        // Effectively disposes the pause button and the loading text
        super.dispose();

        this.unpopped.dispose();
        this.popped1.dispose();
        this.popped2.dispose();
        this.popped3.dispose();
        for(Bubble bubble : bubbles){ bubble.dispose(); }
        this.background.dispose();
    }

    @Override
    public void reset() {
        super.setCurrentDuration(0f);
        this.poppedBubbles = 0;
        for(Bubble bubble : bubbles){ bubble.reset(); }
    }
}
