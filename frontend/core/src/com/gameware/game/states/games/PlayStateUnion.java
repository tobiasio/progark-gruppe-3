package com.gameware.game.states.games;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.ModelInterface;
import com.gameware.game.models.Round;
import com.gameware.game.models.RoundCheck;
import com.gameware.game.models.Tournament;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.sprites.PauseButton;
import com.gameware.game.states.menus.FinishedTournamentState;
import com.gameware.game.GameStateManager;
import com.gameware.game.states.PauseState;
import com.gameware.game.states.menus.ScoreState;
import com.gameware.game.states.menus.SinglePlayerSelectGameState;
import com.gameware.game.states.State;
import com.gameware.game.states.menus.ViewTournamentState;

import java.io.IOException;
import java.util.List;

/*
       The PlayStateUnion class is the Union of all game states.

       If you are going to create a game, it has to extend this class. It has necessary variables
       and methods, such as the gameDone() method which changes to the correct menu state.
       Has the reset method abstract because all games must have a reset method, however it will
       be different for each game.

        Patterns: Union Design, (State)
 */


public abstract class PlayStateUnion extends State {

//    Data
    private Round round = null;
    private Tournament tournament = null;
    private Round updatedRound = null;
    private RoundCheck roundCheck = null;

//    Game values
    private int score;
    protected float totalTimeLimit = 0f;
    protected float currentDuration = 0f;

//    Game objects
    private Texture thumbnail = new Texture(Gdx.files.internal("gameTextures/placeholderThumbnail.png"));
    private PauseButton pauseButton;
    private LoadingText loadingText = new LoadingText();
    private BitmapFont font;

//    Game booleans
    private boolean pauseButtonVisible = true;
    private boolean timeLeftVisible = true;
    private boolean currentScoreVisible = true;


    public PlayStateUnion(GameStateManager gsm){
        super(gsm);

        // Default pause button (black color)
        this.pauseButton = new PauseButton();

        // Default font (black color)
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        font.getData().setScale((float) (Gdx.graphics.getWidth()/GameWare.WIDTH*2.5));
    }

//    Override methods
    @Override
    protected void handleInput(){}

    @Override
    public void update(float dt){
        // Post score and exit if the game is over and the loading text is rendering
        if(this.loadingText.textIsRendering()){
            this.gameDone();
            return;
        }

        // Updates the loading text; nothing happens if there is no loading
        this.loadingText.update(dt);

        // If the game has a total game time (if the game is time-based)
        if(this.totalTimeLimit > 0f) {
            // Increases the current duration, used to keep track of the play duration and stop the game
            // after a while
            this.currentDuration += dt;

            // Set score and start rendering the loading text if the game is over
            if (this.currentDuration > this.totalTimeLimit) {
                this.setGameFinished();
                return;
            }
        }

        // Pause the game if the pause button is visible and was pressed
        if(this.pauseButtonVisible && Gdx.input.justTouched() && this.pauseButton.isPressed(Gdx.input.getX(), Gdx.input.getY())){
            if(GameWare.getInstance().isSoundEffectsOn()){
                pauseResumeBtnSound.play();
            }
            this.gsm.push(new PauseState(this.gsm, this));
        }
    }

    @Override
    public void render(SpriteBatch sb){
        this.loadingText.draw(sb);

        if(this.pauseButtonVisible) {
            this.pauseButton.draw(sb);
        }

        this.renderText(sb);
    }

    @Override
    public void dispose(){
        this.loadingText.dispose();
        this.pauseButton.dispose();
        this.font.dispose();
        this.thumbnail.dispose();
    }

    @Override
    public abstract void reset();


    // Renders the text - This is used by this class' render method as well as by the PauseState
    public void renderText(SpriteBatch sb){
        // Shows the current duration if it's supposed to be visible and the game is time-based
        if(this.timeLeftVisible && this.totalTimeLimit > 0f) {
            sb.begin();
            // Time left
            this.font.draw(sb, "Time: " + String.valueOf(Math.max(Math.round((this.totalTimeLimit - this.currentDuration) * 100), 0.00) / 100.0), Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 20);
            sb.end();
        }

        if(this.currentScoreVisible){
            sb.begin();
            //Score
            this.font.draw(sb, "Score: " + String.valueOf(this.score), Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 100);

            sb.end();
        }
    }


//    Game done methods:

//    When game is done, this method is called and posts the score, ands new instance of the game,
//    uptades rounds and tournaments if in a tournament and sends back to the correct state.
//    Remember to use setScore before using this method.
    public void gameDone(){
        String id = "";
        //Find the id to the game just played
        for (String key : GameWare.getInstance().getGameIdToPlayState().keySet()){
            if ( GameWare.getInstance().getGameIdToPlayState().get(key).getClass() == this.getClass()){
                id = key;
                break;
            }
        }
        try {
            this.postScore(id);
            //Add new instance of the game into the HashMap in GameWare
            GameWare.getInstance().updateGameMap(id, this.getClass().getConstructor(GameStateManager.class).newInstance(gsm));
        }catch(Exception e){
            e.printStackTrace();
        }

        goToNextState();
    }

//    Posts the score
    private void postScore(String gameId )throws IOException {
        if(round==null) {
            QueryIntermediate.postHighscore(GameWare.getInstance().getPlayer().getId(), gameId, this.score);
        } else{
            List<ModelInterface> updatedRoundModels = QueryIntermediate.putRoundScore(round.get_id(),tournament.get_id(), this.score);
            updatedRound = (Round) updatedRoundModels.get(0);
            roundCheck = (RoundCheck) updatedRoundModels.get(1);
        }
    }

//    Go to the correct menu-state to go back to
    private void goToNextState(){
        if(round == null) {
            //Singleplayer
            gsm.set(new ScoreState(gsm, this.score, new SinglePlayerSelectGameState(gsm)));
        } else if(!roundCheck.isActive() || (updatedRound.getTournamentPoints() != 0 && updatedRound.getRoundNr() == tournament.getTotalGames())){
            //Tournament finished
            gsm.set(new ScoreState(gsm, this.score, new FinishedTournamentState(gsm, tournament)));
        } else{
            //Tournament not finished
            if(updatedRound.getTournamentPoints() != 0) {
                try {
                    //Get new round if ready
                    Round nextRound = QueryIntermediate.getRoundFromTournament(tournament.get_id(), GameWare.getInstance().getPlayer().getId(), updatedRound.getRoundNr()+1);
                    gsm.set(new ScoreState(gsm, this.score, new ViewTournamentState(gsm, tournament, nextRound)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                No new round ready
                gsm.set(new ScoreState(gsm, this.score, new ViewTournamentState(gsm, tournament, updatedRound)));
            }
        }
    }

//    Setters
    public void setGameFinished(){
        this.loadingText.setLoading();
    }

    public void setScore(int score){
        this.score = score;
    }

    public void setRound(Round r){
        this.round = r;
    }

    public void setTournament(Tournament t){
        this.tournament = t;
    }

    // Changes the color of the pause button to white
    public void setPauseButtonWhite(){
        this.pauseButton.setButtonWhite();
    }

    // Changes the color of the pause button back to black if it was previously changed to white
    public void setPauseButtonBlack(){
        this.pauseButton.setButtonBlack();
    }

    public void setFontColorWhite(){
        this.font.setColor(Color.WHITE);
    }

    public void setFontColorBlack(){
        this.font.setColor(Color.BLACK);
    }

    public void setFontColorCustom(Color color){
        this.font.setColor(color);
    }

    public void setTotalTimeLimit(float totalTimeLimit){
        this.totalTimeLimit = totalTimeLimit;
    }

    public void setCurrentDuration(float currentDuration){
        this.currentDuration = currentDuration;
    }

    public void setThumbnail(Texture thumbnailTexture){
        this.thumbnail.dispose();
        this.thumbnail = thumbnailTexture;
    }

//    Getters
    public Color getFontColor(){
    return this.font.getColor();
}

    public float getCurrentDuration(){
        return this.currentDuration;
    }

    public float getTotalTimeLimit(){
        return this.totalTimeLimit;
    }

    public Texture getThumbnail(){
        return thumbnail;
    }

//    Hide and show methods:
    public void hidePauseButton(){
        this.pauseButtonVisible = false;
    }

    public void hideTimeLeft(){
        this.timeLeftVisible = false;
    }

    public void hideCurrentScore(){
        this.currentScoreVisible = false;
    }

    public void showPauseButton(){
        this.pauseButtonVisible = true;
    }

    public void showTimeLeft(){
        this.timeLeftVisible = true;
    }

    public void showCurrentScore(){
        this.currentScoreVisible = true;
    }

}

