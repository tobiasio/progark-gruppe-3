package com.gameware.game.states.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.GameWare;
import com.gameware.game.sprites.fruitSlicerSprites.Fruit;
import com.gameware.game.sprites.fruitSlicerSprites.SlicingCircle;
import com.gameware.game.GameStateManager;

import java.util.ArrayList;
import java.util.List;

public class FruitSlicerState extends PlayStateUnion {
    private int totalFruitsCut = 0;
    private float startingEmitFrequency = 1f;
    private float endingEmitFrequency = 0.2f;
    private float timeSinceLastEmit = 0;
    private float lengthMoved = 0f;
    private float slicingSpeedThreshold = Gdx.graphics.getWidth()/20;
    private Texture slicingCircleVFX = new Texture(Gdx.files.internal("gameTextures/FruitSlicer/SlicingVisualEffect.png"));
    private Texture background = new Texture(Gdx.files.internal("gameTextures/FruitSlicer/FruitSlicerBackground.png"));
    private List<Texture> fruitTextures;
    private List<Fruit> emittedFruits;
    private Sound sliceWhooshSound;
    private Sound sliceSquishSound;

    private Vector3 oldTouchPosition = new Vector3(0,0,0);

    private List<SlicingCircle> slicingCircles = new ArrayList<SlicingCircle>();

    public FruitSlicerState(GameStateManager gsm) {
        super(gsm);
        super.setTotalTimeLimit(45f);
        super.setThumbnail(new Texture(Gdx.files.internal("gameTextures/FruitSlicer/FruitSlicerPhotoEdit.png")));
        super.setFontColorWhite();
        super.setPauseButtonWhite();

        this.sliceWhooshSound = Gdx.audio.newSound(Gdx.files.internal("sfx/FruitSlicerWhooshSound.mp3"));
        this.sliceSquishSound = Gdx.audio.newSound(Gdx.files.internal("sfx/FruitSlicerSquishSound.mp3"));

        this.fruitTextures = new ArrayList<>();
        this.emittedFruits = new ArrayList<>();

        for(int textureNum = 1; textureNum <= 20; textureNum++){
            String filename = "gameTextures/FruitSlicer/FruitTexture" + String.valueOf(textureNum) + ".png";
            this.fruitTextures.add(new Texture(Gdx.files.internal(filename)));
        }
    }

    @Override
    public void handleInput(){
        if(Gdx.input.isTouched()) {
            int touchX = Gdx.input.getX();
            int touchY = Gdx.input.getY();
            float deltaTime = Gdx.graphics.getDeltaTime();
            boolean didCut = false;

            this.lengthMoved = (float) Math.sqrt(Math.pow((touchX - this.oldTouchPosition.x), 2) + Math.pow((touchY - this.oldTouchPosition.y), 2));

            // If the user moved fast enough (sliced fast enough)
            if(lengthMoved >= this.slicingSpeedThreshold * (0.01 / deltaTime)) {
                for (Fruit fruit : this.emittedFruits) {
                    if (fruit.isPressed(touchX, touchY)) {
                        this.totalFruitsCut++;
                        didCut = true;
                    }
                }
            }

            if(GameWare.getInstance().isSoundEffectsOn() && didCut){
                this.sliceWhooshSound.play();
                this.sliceSquishSound.play(0.15f);
            }

            super.setScore(this.totalFruitsCut);

            this.oldTouchPosition.x = touchX;
            this.oldTouchPosition.y = touchY;
        }


        // Adds new slicing circles if the user is touching
        if(Gdx.input.isTouched()) {
            this.slicingCircles.add(new SlicingCircle(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY(), this.slicingCircleVFX));
        }
    }

    @Override
    public void update(float dt){
        // Effectively updates the pause button and the loading text
        super.update(dt);

        this.handleInput();

        // Updates the slicing circles
        for(SlicingCircle slicingCircle : this.slicingCircles){
            slicingCircle.update(dt);
        }

        // Disposes the oldest slicing circle if it is disposable
        while(slicingCircles.size() > 0 && slicingCircles.get(0).isDisposable()){
            this.slicingCircles.remove(0).dispose();
        }

        this.timeSinceLastEmit += dt;

        // Increases the emiting frequency towards the endingEmitFrequency as the game gets closer to the end
        float currentFrequency = (float) (this.startingEmitFrequency - (this.startingEmitFrequency - this.endingEmitFrequency) * Math.min(super.getCurrentDuration() / (super.getTotalTimeLimit() * 0.6), 1));

        // Emits a new fruit
        if(this.timeSinceLastEmit > currentFrequency){
            this.emitNewFruit();
            this.timeSinceLastEmit = 0;
        }

        for(Fruit fruit : this.emittedFruits){
            fruit.update(dt);
        }

        // Removes the oldest fruit if it is disposable
        if(this.emittedFruits.size() > 0 && this.emittedFruits.get(0).isDisposable()){
            this.emittedFruits.get(0).dispose();
            this.emittedFruits.remove(this.emittedFruits.get(0));
        }
    }

    @Override
    public void render(SpriteBatch sb){
        sb.begin();

        // Background
        sb.draw(this.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        sb.end();

        // Fruits
        for(Fruit fruit : this.emittedFruits){
            fruit.draw(sb);
        }

        // Slicing circles
        for(SlicingCircle slicingCircle : this.slicingCircles){
            slicingCircle.draw(sb);
        }

        //Effectively renders the pause button, the loading text, the score text and the current duration text
        super.render(sb);
    }

    @Override
    public void dispose(){
        super.dispose();

        this.background.dispose();
        this.slicingCircleVFX.dispose();
        this.sliceWhooshSound.dispose();
        this.sliceSquishSound.dispose();

        for(Texture fruit : this.fruitTextures){
            fruit.dispose();
        }
    }

    public void emitNewFruit() {
        // Four different emit modes: from left, from right, from entire bottom, and from bottom center with different velocity angles
        int emitMode = (int) (Math.random() * 4);
        Fruit fruit;
        Texture fruitTexture = this.fruitTextures.get((int) (Math.random() * this.fruitTextures.size()));
        Vector3 velocity = new Vector3(Gdx.graphics.getWidth() * 3 / 4, 0, 0);

        int x, y, emitAngle;

        float fruitAspectRatio = Float.valueOf(fruitTexture.getWidth()) / Float.valueOf(fruitTexture.getHeight());
        int fruitWidth = Gdx.graphics.getWidth() / 5;
        int fruitHeight = (int) (fruitWidth / fruitAspectRatio);

        // Emit from left
        if (emitMode == 0){
            x = -fruitWidth * 2;
            y = (int) (Math.random() * Gdx.graphics.getHeight() / 3);
            emitAngle = (int) (15 + Math.random() * 60);

            velocity.rotate(emitAngle, 0, 0, 1);

            fruit = new Fruit(x, y, fruitWidth, fruitHeight, velocity, fruitTexture);
            this.emittedFruits.add(fruit);
        }

        // Emit from right
        else if(emitMode == 1) {
            x = Gdx.graphics.getWidth() + fruitWidth * 2;
            y = (int) (Math.random() * Gdx.graphics.getHeight() / 3);
            emitAngle = (int) (180 - (15 + Math.random() * 60));

            velocity.rotate(emitAngle, 0, 0, 1);

            fruit = new Fruit(x, y, fruitWidth, fruitHeight, velocity, fruitTexture);
            this.emittedFruits.add(fruit);
        }

        // Emit from entire bottom part of the screen
        else if(emitMode == 2){
            int xWidth = Gdx.graphics.getWidth() + fruitWidth * 2;
            x = (int)(xWidth*Math.random());
            y = -fruitHeight*2;
            float emitAngleOffset = 30 - 30 * (Float.valueOf(x) / Float.valueOf(xWidth));
            emitAngle = (int) (105 - emitAngleOffset);

            velocity.x = (int) (velocity.x * (1.1 + 0.5 * (15 - Math.abs(90 - Float.valueOf(emitAngle)))/15));
            velocity.rotate(emitAngle, 0, 0, 1);

            fruit = new Fruit(x - fruitWidth, y, fruitWidth, fruitHeight, velocity, fruitTexture);
            this.emittedFruits.add(fruit);
        }

        // Emit from bottom center, with random emit angles
        else if(emitMode == 3){
            x = Gdx.graphics.getWidth()/2 + fruitWidth/2;
            y = - fruitHeight*2;
            emitAngle = (int) (100 - Math.random()*20);

            velocity.x = (int) (velocity.x * 1.4);
            velocity.rotate(emitAngle, 0, 0, 1);

            fruit = new Fruit(x - fruitWidth, y, fruitWidth, fruitHeight, velocity, fruitTexture);
            this.emittedFruits.add(fruit);
        }
    }

    @Override
    public void reset(){
        super.setCurrentDuration(0f);
        this.timeSinceLastEmit = 0f;
        this.totalFruitsCut = 0;
        this.emittedFruits = new ArrayList<>();
    }
}
